
awsacct_cidr_code = {
  default = "010-074-208-000"
  dev     = "010-074-208-000"
  qa      = "010-074-240-000"
  prod    = "010-075-000-000"
}

ally_application_id = "105211"

application_name = "eda"

service_name = "sf-iam"

data_classification = "Proprietary"

owner = "analytics"

issrcl_level = "Low"

scm_project = ""

scm_repo = ""

workspace_prefix = {
  dev  = "dev-f-"
  qa   = "qa-r-"
  prod = ""
}

s3_service_name = "tf-aws-analytics-s3"

s3_bucket_arn_analyst_workspaces = {
  dev  = "arn:aws:s3:::analytics-tf-aws-analyticss3-dev-f-anals3-us-east-1-anals3-1"
  qa   = "arn:aws:s3:::s3bucketnamesyntax"
  prod = "arn:aws:s3:::s3bucketnamesyntax"
}

kms_key_alias = {
  dev  = "alias/eda-tf-aws-analytics-kms-dev-f-innov-analytics-kms"
  qa   = "alias/eda-tf-aws-analytics-kms-qa-r-nonprod-analytics-kms"
  prod = "alias/eda-tf-aws-analytics-kms-prod-analytics-kms"
}

sf_trusted_user_arn = "arn:aws:iam::139316230315:user/6umh-s-v2st3826"

sf_external_id = "ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="

sf_s3_ro_actions = ["s3:GetObject", "s3:GetObjectVersion"]

sf_s3_list_actions = ["s3:ListBucket", "s3:GetBucketLocation"]

sf_s3_rw_actions = [
  "s3:PutObject",
  "s3:GetObject",
  "s3:GetObjectVersion",
  "s3:DeleteObject",
  "s3:DeleteObjectVersion"
]

sf_s3_kms_actions = [
  "kms:Decrypt",
  "kms:Encrypt",
  "kms:GenerateDataKey*",
  "kms:ReEncryptTo",
  "kms:ReEncryptFrom",
  "kms:DescribeKey"
]

user_workspaces = {
  ws_type     = "u"
  root_prefix = "home"
  s3_prefixes = [
    "AROAUHJVHXJWSHYYCJQ2W:X_BZDD56",
    "AROAUHJVHXJWSHYYCJQ2W:X_LZDB6T",
    "AROAUHJVHXJWSHYYCJQ2W:X_RZ5YV3",
    "AROAUHJVHXJWSHYYCJQ2W:X_BZZYHB"
  ]
  sf_external_id = {
    dev  = ["ALLY_SFCRole=5603_9vvrJ7HZTPaq4HTtYy2w8UK/xXA="]
    qa   = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw=q"]
    prod = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw=p"]
  }
}

team_workspaces = {
  ws_type     = "t"
  root_prefix = "team"
  team_configs = {
    TECH-PLTFRM-AWS = {
      name = "TECH-PLTFRM-AWS"
      env_scope = {
        dev  = ["innov", "test"]
        qa   = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_PLATFORM_AWS", "CUMULUS"]
      sf_ro_external_id = {
        dev  = ["ALLY_SFCRole=5603_ouUKf1C/r0ofID5VtuQ8LVqBSIQ="]
        qa   = ["ALLY_SFCRole=5603_ovz1bhZv6z0ilo7+z3gJJeEnk2U="]
        prod = ["ALLY_SFCRole=5603_rIWflOvihKKnuqvrPXnVEbzzDc0="]
      }
      sf_rw_external_id = {
        dev  = ["ALLY_SFCRole=5603_WBV3pdzGd5smyyyG1X3m8cJU3mY="]
        qa   = ["ALLY_SFCRole=5603_DElk/1yrMj0s+C+wmniMMXVoiGY="]
        prod = ["ALLY_SFCRole=5603_KQ7IwRLd3v2+ZpnVBf7/S6Xc7rc="]
      }
    }
    AUTO-BUS-OPTI = {
      name = "AUTO-BUS-OPTI"
      env_scope = {
        dev  = []
        qa   = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_BUSINESS_OPTIMIZATION"]
      sf_ro_external_id = {
        dev  = []
        qa   = []
        prod = ["ALLY_SFCRole=5603_D4z7hidprXDEFNKKNNhBFaejTL8="]
      }
      sf_rw_external_id = {
        dev  = []
        qa   = []
        prod = ["ALLY_SFCRole=5603_RAwG/+EgVMacnYmJmyz3MHJ7iFs="]
      }
    }
    AUTO-DSS = {
      name = "AUTO-DSS"
      env_scope = {
        dev  = []
        qa   = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS"]
      sf_ro_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = ["ALLY_SFCRole=5603_JUPre8DJwun6ylHRumUzzMTjS08="]
        prod = ["ALLY_SFCRole=5603_6hZQJ3xl9Kh4sDv5tsNiRyf1PGw="]
      }
      sf_rw_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = ["ALLY_SFCRole=5603_jaCDcNUOekV3LFofN014khqiXZ0="]
        prod = ["ALLY_SFCRole=5603_2EBYwPZmxYLzEMcf9e+BY30Ew28="]
      }
    }
    AUTO-DSS-CS-LP = {
      name = "AUTO-DSS-CS-LP"
      env_scope = {
        dev  = []
        qa   = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS_CAM_CS_LP"]
      sf_ro_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = []
        prod = ["ALLY_SFCRole=5603_/0Xz4piFl0KiGQuYbjLrv0yOjqY="]
      }
      sf_rw_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = []
        prod = ["ALLY_SFCRole=5603_CuJp/oCjfT9jFxLt9m0rNvrgro8="]
      }
    }
    AUTO-DSS-DIG = {
      name = "AUTO-DSS-DIG"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS_DIGITAL"]
      sf_ro_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = [
          "ALLY_SFCRole=5603_oW5Lsu0+s3/Q6Dtc3GKK62FGAPc=",
          "ALLY_SFCRole=5603_rPLwhGjKckR6rPhppw0xNXrSQr4=",
          "ALLY_SFCRole=5603_2KsFSps5VWSgud0LhwODm0TZmYg=",
          "ALLY_SFCRole=5603_8U8IWWe1fDcCupp/UgKJtM8RMC8="
        ]
        prod = ["ALLY_SFCRole=5603_XonTS21drz5XnpASvr3D4sHlXOQ="]
      }
      sf_rw_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = [
          "ALLY_SFCRole=5603_qrIOaLD0kbgHNS5rhMBn1jdtOMA=",
          "ALLY_SFCRole=5603_bT9hgzgKe6cznd++mUaIY/9B314=",
          "ALLY_SFCRole=5603_Jfh0nAZd9YIKM6uZ4UmQQU8kIpQ=",
          "ALLY_SFCRole=5603_00GWOInk2hxPXjHzcUpfBh4u+cA="
        ]
        prod = ["ALLY_SFCRole=5603_GUBQwP5MOY9dBe4hy5DmYmXXNQA="]
      }
    }
    AUTO-INSU-SPA = {
      name = "AUTO-INSU-SPA"
      env_scope = {
        dev  = []
        qa   = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_INSURANCE_SPA"]
      sf_ro_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = []
        prod = ["ALLY_SFCRole=5603_hK1VeeX2B8hlDok2hrfdx+vmkhA="]
      }
      sf_rw_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = []
        prod = ["ALLY_SFCRole=5603_q/MakwrWLMs0E/dJv45rPal/ubk="]
      }
    }
    TECH-CIH = {
      name = "TECH-CIH"
      env_scope = {
        dev  = []
        qa   = ["dev", "sit"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_CIH"]
      sf_ro_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = ["ALLY_SFCRole=5603_EP9w1+rR/McKkqye3/hauH/Q7Dc=", "ALLY_SFCRole=5603_vXhakboTQMGZ4msBN+P31+MdAIY="]
        prod = ["ALLY_SFCRole=5603_RanWC2rr3hf1+NKSILzqfuw/Aig="]
      }
      sf_rw_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = ["ALLY_SFCRole=5603_RSB5MHXjDVI5iUZZstn7GNYGPWM=", "ALLY_SFCRole=5603_IvC5rh9Q3+KBcKj7pAsudRXlZSQ="]
        prod = ["ALLY_SFCRole=5603_IVe1RpmOqPt9LngTyYytn7Hh0c4="]
      }
    }
    TECH-QMA = {
      name = "TECH-QMA"
      env_scope = {
        dev  = []
        qa   = []
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_QMA"]
      sf_ro_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = []
        prod = ["ALLY_SFCRole=5603_MPBUuFZxe2IjnedxSgjuBdiuxh4="]
      }
      sf_rw_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = []
        prod = ["ALLY_SFCRole=5603_O/3xEmrYGJr+SxU+0RqqqFwhaUk="]
      }
    }
    TECH-MEAD = {
      name = "TECH-MEAD"
      env_scope = {
        dev  = []
        qa   = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_MEAD"]
      sf_ro_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = ["ALLY_SFCRole=5603_dC+kJmnG+38/obnioafmWkwsWDE="]
        prod = ["ALLY_SFCRole=5603_ai/EJ+ntxumKeH/BReW0Tgwk48s="]
      }
      sf_rw_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = ["ALLY_SFCRole=5603_5vwmaFm7nXkOwPJtfm0TdCm/cx8="]
        prod = ["ALLY_SFCRole=5603_z5JcPq3eMiFW1WDc3LYcY2RkUrg="]
      }
    }
    TECH-PLTFRM-NUC = {
      name = "TECH-PLTFRM-NUC"
      env_scope = {
        dev  = []
        qa   = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_PLATFORM_NUCLEUS"]
      sf_ro_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = ["ALLY_SFCRole=5603_LNMPeQMR3JAmr9Zpxt05JQIVGuU="]
        prod = ["ALLY_SFCRole=5603_Gq77ABRuX5MzVnlQJc7xk9i9pto="]
      }
      sf_rw_external_id = {
        dev  = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa   = ["ALLY_SFCRole=5603_wYvCWUW+qMjE2w41dfH5J23nKxM="]
        prod = ["ALLY_SFCRole=5603_C8VWMwlw3HmJDyQgPsLxKPKwHb4="]
      }
    }
  }
}
