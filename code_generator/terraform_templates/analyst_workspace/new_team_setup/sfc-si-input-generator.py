#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, csv

# Function which initializes the variables needed for execution
def initialize_variables():
  global program_params, out_data

  program_params = {
    'params_file_name': 'analyst-new-team-setup.param',
    'out_file_name_si': {
      'nonprod': 'sfc-si-input-nonprod.csv',
      'prod': 'sfc-si-input-prod.csv'
    },
    'out_file_name_es': {
      'nonprod': 'sfc-es-input-nonprod.csv',
      'prod': 'sfc-es-input-prod.csv'
    },
    'env_map': {
      'I': {'env_name': 'dev', 'sdlc_name': 'innov'},
      'D': {'env_name': 'qa', 'sdlc_name': 'dev'},
      'S': {'env_name': 'qa', 'sdlc_name': 'sit'},
      'M': {'env_name': 'qa', 'sdlc_name': 'model'},
      'C': {'env_name': 'qa', 'sdlc_name': 'cap'},
      'P': {'env_name': 'prod', 'sdlc_name': 'prod'}
    },
    's3_sub_prefixes': ['WORK', 'TEST', 'CORE', 'STATIC']
  }
  out_data = ''


# Main function that is invoked first
def main():
  global program_params

  initialize_variables()

  # Read the input paramters in a list of dictionaries
  params_list = []
  with open(program_params['params_file_name'], 'r') as param_file:
    for params in csv.DictReader(param_file):
      params_list.append(params)

  # NonProd SI
  with open(program_params['out_file_name_si']['nonprod'], 'w') as outfile:
    for params in params_list:
      team_name = params['team_name']
      sfc_team_name = 'TEAM_' + team_name
      team_short_name = params['team_short_name']
      env_scope = params['env_names'].split(',')

      for env_short_name in env_scope:
        sdlc_name = program_params['env_map'][env_short_name]['sdlc_name']
        if env_short_name in ['D','S','M','C']:
          aws_account_id = '894431826386'
          env_type_prefix = 'qa-r-'
          env_type = 'nonprod'
          s3_arn = 's3://eda-tf-aws-analytics-s3-' + env_type_prefix + sdlc_name + '-us-east-1-workspaces/team/' + sfc_team_name + '_' + env_short_name + '/'
          for access_type in ['RO', 'RW']:
            sfc_iam_role_arn = 'arn:aws:iam::' + aws_account_id + ':role/eda-sf-iam-' + env_type_prefix + env_type + '-us-east-1-t-' + team_short_name + '-analyst-' + access_type.lower()
            outfile.write(sfc_team_name + ',' + access_type + ',' + env_short_name + ',' + sfc_iam_role_arn + ',' + s3_arn + ',' + team_name)
            outfile.write('\n')

  # Prod SI
  with open(program_params['out_file_name_si']['prod'], 'w') as outfile:
    for params in params_list:
      team_name = params['team_name']
      sfc_team_name = 'TEAM_' + team_name
      team_short_name = params['team_short_name']
      env_scope = params['env_names'].split(',')

      for env_short_name in env_scope:
        sdlc_name = program_params['env_map'][env_short_name]['sdlc_name']
        if env_short_name == 'P':
          aws_account_id = '777181025923'
          env_type_prefix = ''
          env_type = 'prod'
          s3_arn = 's3://eda-tf-aws-analytics-s3-' + env_type_prefix + sdlc_name + '-us-east-1-workspaces/team/' + sfc_team_name + '_' + env_short_name + '/'
          for access_type in ['RO', 'RW']:
            sfc_iam_role_arn = 'arn:aws:iam::' + aws_account_id + ':role/eda-sf-iam-' + env_type_prefix + env_type + '-us-east-1-t-' + team_short_name + '-analyst-' + access_type.lower()
            outfile.write(sfc_team_name + ',' + access_type + ',' + env_short_name + ',' + sfc_iam_role_arn + ',' + s3_arn + ',' + team_name)
            outfile.write('\n')

  # NonProd External Stage
  with open(program_params['out_file_name_es']['nonprod'], 'w') as outfile:
    for params in params_list:
      team_name = params['team_name']
      sfc_team_name = 'TEAM_' + team_name
      env_scope = params['env_names'].split(',')

      for env_short_name in env_scope:
        sdlc_name = program_params['env_map'][env_short_name]['sdlc_name']
        if env_short_name in ['D','S','M','C']:
          env_type_prefix = 'qa-r-'
          kms_key_id = 'e1821f16-b8d3-471e-b01f-11bd3e85c8fe'
          for access_type in ['RO', 'RW']:
            for s3_sub_prefix in program_params['s3_sub_prefixes']:
              s3_arn = 's3://eda-tf-aws-analytics-s3-' + env_type_prefix + sdlc_name + '-us-east-1-workspaces/team/' + sfc_team_name + '_' + env_short_name + '/' + s3_sub_prefix + '/'
              outfile.write(sfc_team_name + ',' + s3_sub_prefix + ',' + access_type + ',' + env_short_name + ',' + s3_arn + ',' + kms_key_id)
              outfile.write('\n')

  # Prod External Stage
  with open(program_params['out_file_name_es']['prod'], 'w') as outfile:
    for params in params_list:
      team_name = params['team_name']
      sfc_team_name = 'TEAM_' + team_name
      env_scope = params['env_names'].split(',')

      for env_short_name in env_scope:
        sdlc_name = program_params['env_map'][env_short_name]['sdlc_name']
        if env_short_name == 'P':
          env_type_prefix = ''
          kms_key_id = '68a2c6ee-98ce-44f2-b33c-f85dc80682ea'
          for access_type in ['RO', 'RW']:
            for s3_sub_prefix in program_params['s3_sub_prefixes']:
              s3_arn = 's3://eda-tf-aws-analytics-s3-' + env_type_prefix + sdlc_name + '-us-east-1-workspaces/team/' + sfc_team_name + '_' + env_short_name + '/' + s3_sub_prefix + '/'
              outfile.write(sfc_team_name + ',' + s3_sub_prefix + ',' + access_type + ',' + env_short_name + ',' + s3_arn + ',' + kms_key_id)
              outfile.write('\n')


# Python program execution begins here
if __name__ == "__main__":
  main()
