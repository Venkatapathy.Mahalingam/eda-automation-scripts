
awsacct_cidr_code = {
  default = "010-074-208-000"
  dev = "010-074-208-000"
  qa = "010-074-240-000"
  prod = "010-075-000-000"
}

ally_application_id = "105211"

application_name = "eda"

service_name = "tf-aws-analytics-s3"

data_classification = "Proprietary"

owner = "analytics"

issrcl_level = "Low"

scm_project = ""

scm_repo = ""

s3_env = {
  dev = "dev"
}

kms_key_alias = {
  dev = "alias/eda-tf-aws-analytics-kms-dev-f-innov-analytics-kms"
  qa = "alias/eda-tf-aws-analytics-kms-qa-r-nonprod-analytics-kms"
  prod = "alias/eda-tf-aws-analytics-kms-prod-analytics-kms"
}

iam_role_id = {
  dev = "AROAUHJVHXJWSHYYCJQ2W"
  qa = "AROA5AQCKSHJJM42CXO4H"
  prod = "AROA3J44ZK2BQ4MTG2FX7"
}

user_workspaces = {
  ws_type = "u"
  root_prefix = "user"
  s3_prefixes = [
    "X_BZ368G",
    "X_BZ6YZS",
    "X_BZDD56",
    "X_BZJX70",
    "X_BZL8Z8",
    "X_BZZYHB",
    "X_CZ0TCS",
    "X_CZ7QRX",
    "X_CZNWNB",
    "X_CZQJZK",
    "X_DZ2DJL",
    "X_DZ9BMW",
    "X_DZF6BT",
    "X_DZNXBV",
    "X_DZP6D6",
    "X_DZPM5K",
    "X_DZSY6W",
    "X_FZ3TNZ",
    "X_FZ748C",
    "X_FZBC3B",
    "X_FZCRZB",
    "X_FZH76Y",
    "X_FZJHZZ",
    "X_FZS6SW",
    "X_FZZC7N",
    "X_GZ0V5B",
    "X_GZBQ7P",
    "X_GZGR10",
    "X_GZP0WW",
    "X_GZXL2V",
    "X_GZYFCW",
    "X_HZ0X71",
    "X_HZ1XGP",
    "X_HZ2N26",
    "X_HZ361X",
    "X_HZ73X5",
    "X_HZ7660",
    "X_HZ89ZX",
    "X_HZ980R",
    "X_HZ9YQL",
    "X_HZB806",
    "X_HZC0BW",
    "X_HZHMDJ",
    "X_HZP29H",
    "X_HZYHTP",
    "X_JZ3LWW",
    "X_JZ6NX9",
    "X_JZCSVJ",
    "X_JZLM3G",
    "X_JZVGKX",
    "X_JZYKT2",
    "X_KZ2VPS",
    "X_KZ2Z10",
    "X_KZ60HC",
    "X_KZ8CHV",
    "X_KZCY3M",
    "X_KZGR84",
    "X_KZMQ8T",
    "X_LZ0MV3",
    "X_LZ38C0",
    "X_LZ3T06",
    "X_LZ6JNW",
    "X_LZ7NNZ",
    "X_LZB0Z3",
    "X_LZDB6T",
    "X_LZJZ1Q",
    "X_LZYWKP",
    "X_MZ3GX7",
    "X_MZ6NYX",
    "X_MZ7SC1",
    "X_MZJRGP",
    "X_MZKCK3",
    "X_MZLDQK",
    "X_MZQ0VV",
    "X_MZZZGQ",
    "X_NZ0J1X",
    "X_NZ2CJH",
    "X_NZ2VPS",
    "X_NZ3TTX",
    "X_NZ75H4",
    "X_NZGF82",
    "X_NZLQFR",
    "X_NZMBYS",
    "X_NZVZWM",
    "X_NZXSMQ",
    "X_NZZ91N",
    "X_PZ244F",
    "X_PZ6HZT",
    "X_PZ8SLK",
    "X_PZJ254",
    "X_PZJ3P5",
    "X_PZK8QC",
    "X_PZM1SJ",
    "X_PZM31D",
    "X_PZMVFK",
    "X_PZSLKS",
    "X_PZTVN8",
    "X_PZW7V7",
    "X_PZZDXJ",
    "X_QZ2ZLN",
    "X_QZ60ZR",
    "X_QZD47C",
    "X_QZFWJL",
    "X_QZH4DV",
    "X_QZKL57",
    "X_QZL21F",
    "X_QZLN56",
    "X_QZP026",
    "X_QZS215",
    "X_QZT8ZY",
    "X_QZTHFH",
    "X_RZ5YV3",
    "X_RZ85SF",
    "X_RZ94HV",
    "X_RZ9FHY",
    "X_RZ9GLM",
    "X_RZC84R",
    "X_RZQQC9",
    "X_RZS4LR",
    "X_RZWHM5",
    "X_RZXPB8",
    "X_SZ07Z9",
    "X_SZ0RMD",
    "X_SZB8KS",
    "X_SZCSRK",
    "X_SZCXZQ",
    "X_SZD9C5",
    "X_SZDQ69",
    "X_SZFBST",
    "X_SZFC5H",
    "X_SZJ2KG",
    "X_SZJ30M",
    "X_SZJXHK",
    "X_SZPWM9",
    "X_TZ00ND",
    "X_TZ57QZ",
    "X_TZ6244",
    "X_TZ9R72",
    "X_TZBTNC",
    "X_TZDGYJ",
    "X_TZKFSX",
    "X_TZQ3DT",
    "X_TZSYZD",
    "X_TZVTDC",
    "X_VZ0KRK",
    "X_VZ6G8X",
    "X_VZ88FZ",
    "X_VZ9R4W",
    "X_VZBJ66",
    "X_VZDSPX",
    "X_VZHVYQ",
    "X_VZM6JN",
    "X_VZWFFN",
    "X_VZYWF2",
    "X_WZ0203",
    "X_WZKD2W",
    "X_WZKV49",
    "X_WZS4K1",
    "X_WZV72Q",
    "X_XZ0XH2",
    "X_XZ3MFB",
    "X_XZ51PJ",
    "X_XZ9XRW",
    "X_XZJ5BC",
    "X_XZTKK0",
    "X_XZTX1V",
    "X_ZZ2QL6",
    "X_ZZ6CXJ",
    "X_ZZ6L7C",
    "X_ZZB08G",
    "X_ZZQ6K0",
    "X_ZZVMWQ"
  ]
}

team_workspaces = {
  ws_type = "t"
  root_prefix = "team"
  team_configs = {
    TECH-PLTFRM-AWS = {
      name = "TECH-PLTFRM-AWS"
      env_scope = [
        "innov",
        "dev",
        "prod"
      ]
      s3_prefixes = ["TECH_PLATFORM_AWS"]
    }
    AUTO-BUS-OPTI = {
      name = "AUTO-BUS-OPTI"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_BUSINESS_OPTIMIZATION"]
    }
    AUTO-DSS = {
      name = "AUTO-DSS"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["AUTO_DSS"]
    }
    AUTO-DSS-CS-LP = {
      name = "AUTO-DSS-CS-LP"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DSS_CAM_CS_LP"]
    }
    AUTO-DSS-DIG = {
      name = "AUTO-DSS-DIG"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["AUTO_DSS_DIGITAL"]
    }
    AUTO-INSU-SPA = {
      name = "AUTO-INSU-SPA"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_INSURANCE_SPA"]
    }
    TECH-CIH = {
      name = "TECH-CIH"
      env_scope = [
        "dev",
        "sit",
        "prod"
      ]
      s3_prefixes = ["TECH_CIH"]
    }
    TECH-QMA = {
      name = "TECH-QMA"
      env_scope = ["prod"]
      s3_prefixes = ["TECH_QMA"]
    }
    TECH-MEAD = {
      name = "TECH-MEAD"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_MEAD"]
    }
    TECH-PLTFRM-NUC = {
      name = "TECH-PLTFRM-NUC"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_PLATFORM_NUCLEUS"]
    }
    ATA-BTCMP = {
      name = "ATA-BTCMP"
      env_scope = ["dev"]
      s3_prefixes = ["ATA_BOOTCAMP"]
    }
    AUDT-SER = {
      name = "AUDT-SER"
      env_scope = ["prod"]
      s3_prefixes = ["AUDIT_SERVICES"]
    }
    AUTO-ADV-OPTI = {
      name = "AUTO-ADV-OPTI"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_ADVANCED_PROCESS_OPTIMIZATION"]
    }
    AUTO-AUTOVAL = {
      name = "AUTO-AUTOVAL"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["AUTO_AUTOVAL"]
    }
    AUTO-CAM-STR = {
      name = "AUTO-CAM-STR"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_CAM_STRATEGY"]
    }
    AUTO-CL-BLCR = {
      name = "AUTO-CL-BLCR"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_CL_BLCR"]
    }
    AUTO-COSA = {
      name = "AUTO-COSA"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA"]
    }
    AUTO-COSA-AA = {
      name = "AUTO-COSA-AA"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_AA"]
    }
    AUTO-COSA-ADS = {
      name = "AUTO-COSA-ADS"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_ADS"]
    }
    AUTO-COSA-CCO = {
      name = "AUTO-COSA-CCO"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_CCO"]
    }
    AUTO-COSA-CP = {
      name = "AUTO-COSA-CP"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_CP"]
    }
    AUTO-COSA-NDLDG = {
      name = "AUTO-COSA-NDLDG"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_NDLDG"]
    }
    AUTO-COSA-PRM = {
      name = "AUTO-COSA-PRM"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_PRM"]
    }
    AUTO-COSA-SASO = {
      name = "AUTO-COSA-SASO"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_SASO"]
    }
    AUTO-CRD-OPS = {
      name = "AUTO-CRD-OPS"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_CREDIT_OPS"]
    }
    AUTO-DIR = {
      name = "AUTO-DIR"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DIRECT"]
    }
    AUTO-DLR-MGMT = {
      name = "AUTO-DLR-MGMT"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DLR_MGMT"]
    }
    AUTO-DSS-ARC-SSO = {
      name = "AUTO-DSS-ARC-SSO"
      env_scope = [
        "dev",
        "sit",
        "cap",
        "prod"
      ]
      s3_prefixes = ["AUTO_DSS_ARC_SSO"]
    }
    AUTO-DSS-CON-STR = {
      name = "AUTO-DSS-CON-STR"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["AUTO_DSS_CONS_STRATEGY"]
    }
    AUTO-DSS-DA = {
      name = "AUTO-DSS-DA"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DSS_DA"]
    }
    AUTO-DSS-DIR = {
      name = "AUTO-DSS-DIR"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DSS_DIRECT"]
    }
    AUTO-DSS-ORIG = {
      name = "AUTO-DSS-ORIG"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DSS_ORIG"]
    }
    AUTO-DSS-SLS = {
      name = "AUTO-DSS-SLS"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["AUTO_DSS_SLS"]
    }
    AUTO-DSS-UW = {
      name = "AUTO-DSS-UW"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DSS_UW"]
    }
    AUTO-ICP = {
      name = "AUTO-ICP"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_ICP"]
    }
    AUTO-PRTFL-RSK = {
      name = "AUTO-PRTFL-RSK"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_PORTFOLIO_RISK"]
    }
    AUTO-PRC = {
      name = "AUTO-PRC"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_PRICING"]
    }
    AUTO-RMRKT-3PR = {
      name = "AUTO-RMRKT-3PR"
      env_scope = ["model", "prod"]
      s3_prefixes = ["AUTO_REMARKETING_3PR"]
    }
    AUTO-RSK-FRCST = {
      name = "AUTO-RSK-FRCST"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_RISK_FORECAST"]
    }
    AUTO-SLSALLNCE = {
      name = "AUTO-SLSALLNCE"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_SALESALLIANCE"]
    }
    AUTO-SP-ADR = {
      name = "AUTO-SP-ADR"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_SP_ADR"]
    }
    AUTO-SP-ICP = {
      name = "AUTO-SP-ICP"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_SP_ICP"]
    }
    BANK-CCERA = {
      name = "BANK-CCERA"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["BANK_CCERA"]
    }
    BANK-CON = {
      name = "BANK-CON"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["BANK_CONSUMER"]
    }
    BANK-CX-CC = {
      name = "BANK-CX-CC"
      env_scope = ["model", "prod"]
      s3_prefixes = ["BANK_CX_CC"]
    }
    BANK-DEP-OPS = {
      name = "BANK-DEP-OPS"
      env_scope = ["model", "prod"]
      s3_prefixes = ["BANK_DEPOSIT_OPS"]
    }
    BANK-DSS-DEPINV = {
      name = "BANK-DSS-DEPINV"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["BANK_DSS_DEPINV"]
    }
    BANK-FRD-STR = {
      name = "BANK-FRD-STR"
      env_scope = ["model", "prod"]
      s3_prefixes = ["BANK_FRAUD_STRATEGY"]
    }
    BANK-INV = {
      name = "BANK-INV"
      env_scope = ["model", "prod"]
      s3_prefixes = ["BANK_INVEST"]
    }
    BANK-MORT-BA = {
      name = "BANK-MORT-BA"
      env_scope = ["prod"]
      s3_prefixes = ["BANK_MORT_BA"]
    }
    COMP-AML = {
      name = "COMP-AML"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["COMPLIANCE_AML"]
    }
    COMP-EFSI = {
      name = "COMP-EFSI"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["COMPLIANCE_EFSI"]
    }
    FIN-ACC-ENG = {
      name = "FIN-ACC-ENG"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["FINANCE_ACCOUNTING_ENG"]
    }
    FIN-AUTO-CFO = {
      name = "FIN-AUTO-CFO"
      env_scope = ["prod"]
      s3_prefixes = ["FINANCE_AUTO_CFO"]
    }
    FIN-CPTL-MRKTS = {
      name = "FIN-CPTL-MRKTS"
      env_scope = [
        "dev",
        "model",
        "prod"
      ]
      s3_prefixes = ["FINANCE_CPTL_MARKETS"]
    }
    FIN-ECNMCS = {
      name = "FIN-ECNMCS"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["FINANCE_ECONOMICS"]
    }
    FIN-FMA-MDL = {
      name = "FIN-FMA-MDL"
      env_scope = ["prod"]
      s3_prefixes = ["FINANCE_FMA_MODEL"]
    }
    FIN-GL-SAP = {
      name = "FIN-GL-SAP"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["FINANCE_GL_SAP"]
    }
    FIN-SPLY-CHN = {
      name = "FIN-SPLY-CHN"
      env_scope = [
        "dev",
        "model",
        "prod"
      ]
      s3_prefixes = ["FINANCE_SUPPLY_CHAIN"]
    }
    FIN-VAR-LIB = {
      name = "FIN-VAR-LIB"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["FINANCE_VARIABLES_LIBRARY"]
    }
    HR-PEP-SYS = {
      name = "HR-PEP-SYS"
      env_scope = ["prod"]
      s3_prefixes = ["HR_PEOPLE_SYSTEMS"]
    }
    HR-WRKFRC = {
      name = "HR-WRKFRC"
      env_scope = ["prod"]
      s3_prefixes = ["HR_WORKFORCE"]
    }
    INS-DWR = {
      name = "INS-DWR"
      env_scope = ["model", "prod"]
      s3_prefixes = ["INSURANCE_DWR"]
    }
    IRM-AUTO-CRD = {
      name = "IRM-AUTO-CRD"
      env_scope = ["prod"]
      s3_prefixes = ["IRM_AUTO_CREDIT"]
    }
    IRM-CARD = {
      name = "IRM-CARD"
      env_scope = ["prod"]
      s3_prefixes = ["IRM_CARD"]
    }
    IRM-ESG = {
      name = "IRM-ESG"
      env_scope = ["prod"]
      s3_prefixes = ["IRM_ESG"]
    }
    IRM-INS-RSK = {
      name = "IRM-INS-RSK"
      env_scope = ["prod"]
      s3_prefixes = ["IRM_INSURANCE_RISK"]
    }
    MKTG-CRM = {
      name = "MKTG-CRM"
      env_scope = ["prod"]
      s3_prefixes = ["MARKETING_CRM"]
    }
    MKTG-DIG = {
      name = "MKTG-DIG"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["MARKETING_DIGITAL"]
    }
    TECH-ADV = {
      name = "TECH-ADV"
      env_scope = ["prod"]
      s3_prefixes = ["TECH_ADVANTAGE"]
    }
    TECH-COMP-CRT = {
      name = "TECH-COMP-CRT"
      env_scope = ["dev"]
      s3_prefixes = ["TECH_COMPLIANCE_CRT"]
    }
    TECH-COMP-DENG = {
      name = "TECH-COMP-DENG"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["TECH_COMPLIANCE_DATAENG"]
    }
    TECH-COMP-FRD-CM = {
      name = "TECH-COMP-FRD-CM"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["TECH_COMPLIANCE_FRAUD_CASEMGMT"]
    }
    TECH-COMP-SUS = {
      name = "TECH-COMP-SUS"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["TECH_COMPLIANCE_SUSTAIN"]
    }
    TECH-CONVOAI = {
      name = "TECH-CONVOAI"
      env_scope = ["prod"]
      s3_prefixes = ["TECH_CONVOAI"]
    }
    TECH-DGO = {
      name = "TECH-DGO"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_DGO"]
    }
    TECH-EA-DATA = {
      name = "TECH-EA-DATA"
      env_scope = ["prod"]
      s3_prefixes = ["TECH_EA_DATA"]
    }
    TECH-EDA-BD-SER = {
      name = "TECH-EDA-BD-SER"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["TECH_EDA_BANK_DATA_SERVICES"]
    }
    TECH-EDA-SOL-ENG = {
      name = "TECH-EDA-SOL-ENG"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_EDA_SOLUTION_ENG"]
    }
    TECH-FUS = {
      name = "TECH-FUS"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_FUSION"]
    }
    TECH-IPRM-CRSK = {
      name = "TECH-IPRM-CRSK"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_IPRM_CYBERRISK"]
    }
    TECH-SUS-EDA = {
      name = "TECH-SUS-EDA"
      env_scope = ["dev"]
      s3_prefixes = ["TECH_SUSTAIN_EDA"]
    }
  }
}

s3_sub_prefixes = [
  "WORK",
  "TEST",
  "CORE",
  "STATIC"
]
