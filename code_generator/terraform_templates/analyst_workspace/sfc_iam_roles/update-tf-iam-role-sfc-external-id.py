#!/usr/bin/env python
# -*- coding: utf-8 -*-

#from operator import index
import os, csv
import hcl, json

# Function which initializes the variables needed for execution
def initialize_variables():
  global program_params, out_data

  program_params = {
    'params_file_name': 'iam-role-sfc-external-id.param',
    'tf_file_name': 'terraform.tfvars',
    'out_file_name': 'output.tfvars',
    'env_map': {
      'D': 'qa',
      'S': 'qa',
      'M': 'qa',
      'C': 'qa',
      'P': 'prod'
    }
  }
  out_data = ''


# Function to update Terraform vars dictionary with the input External IDs
def update_tf_vars(tf_data, params_list):
  global program_params

  for params in params_list:
    team_name = params['team_short_name']
    sfc_ext_id_type = 'sf_' + params['access_type'].lower() + '_external_id'
    env_name = program_params['env_map'][params['env_names']]
    sfc_ext_id = params['external_id']

    tf_data['team_workspaces']['team_configs'][team_name][sfc_ext_id_type][env_name].append(sfc_ext_id)

  return tf_data


# Function to convert Terraform vars dictionary to .tfvars file
def convert_dict2hcl(tf_data, indent):
  global program_params, out_data

  spaces = indent * ' '
  for tf_var_name, tf_var_value in tf_data.items():
    if isinstance(tf_var_value, str):
      out_data = out_data + '\n' + spaces + tf_var_name + ' = "' + tf_var_value + '"'
    elif isinstance(tf_var_value, list):
      list_size = len(tf_var_value)
      if list_size > 2:
        out_data = out_data + '\n' + spaces + tf_var_name + ' = ['
        spaces = (indent + 2) * ' '
        for list_index, list_item in enumerate(tf_var_value, start=1):
          out_data = out_data + '\n' + spaces + list_item.join(['"', '"'])
          if list_index < list_size:
            out_data = out_data + ','
        spaces = indent * ' '
        out_data = out_data + '\n' + spaces + ']'
      elif list_size == 0:
        out_data = out_data + '\n' + spaces + tf_var_name + ' = []'
      else:
        out_data = out_data + '\n' + spaces + tf_var_name + ' = ["' + '", "'.join(tf_var_value) + '"]'
    elif isinstance(tf_var_value, dict):
      out_data = out_data + '\n' + spaces + tf_var_name + ' = {'
      indent = indent + 2
      convert_dict2hcl(tf_var_value, indent)
      out_data = out_data + '\n' + spaces + '}'
      indent = indent - 2

    if indent == 0:
      out_data = out_data + '\n'

  with open(program_params['out_file_name'], 'w') as outfile:
    outfile.write(out_data)


# Main function that is invoked first
def main():
  global program_params

  initialize_variables()

  # Convert TF to DICT
  tf_data = {}
  with open(program_params['tf_file_name']) as tf_file:
    tf_data = hcl.load(tf_file)

  # Read the input parameters
  params_list = []
  with open(program_params['params_file_name']) as params_file:
    for input_row in csv.DictReader(params_file):
      params_list.append(input_row)

  # Update the DICT based on the input parameters
  tf_data = update_tf_vars(tf_data, params_list)

  # Save the output in .tfvars format
  convert_dict2hcl(tf_data, indent=0)


# Python program execution begins here
if __name__ == "__main__":
  main()
