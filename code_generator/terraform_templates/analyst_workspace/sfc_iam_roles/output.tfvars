
awsacct_cidr_code = {
  default = "010-074-208-000"
  dev = "010-074-208-000"
  qa = "010-074-240-000"
  prod = "010-075-000-000"
}

ally_application_id = "105211"

application_name = "eda"

service_name = "sf-iam"

data_classification = "Proprietary"

owner = "analytics"

issrcl_level = "Low"

scm_project = ""

scm_repo = ""

workspace_prefix = {
  dev = "dev-f-"
  qa = "qa-r-"
  prod = ""
}

s3_service_name = "tf-aws-analytics-s3"

s3_bucket_arn_analyst_workspaces = {
  dev = "arn:aws:s3:::analytics-tf-aws-analyticss3-dev-f-anals3-us-east-1-anals3-1"
  qa = "arn:aws:s3:::s3bucketnamesyntax"
  prod = "arn:aws:s3:::s3bucketnamesyntax"
}

kms_key_alias = {
  dev = "alias/eda-tf-aws-analytics-kms-dev-f-innov-analytics-kms"
  qa = "alias/eda-tf-aws-analytics-kms-qa-r-nonprod-analytics-kms"
  prod = "alias/eda-tf-aws-analytics-kms-prod-analytics-kms"
}

sf_trusted_user_arn = "arn:aws:iam::139316230315:user/6umh-s-v2st3826"

sf_external_id = "ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="

sf_s3_ro_actions = ["s3:GetObject", "s3:GetObjectVersion"]

sf_s3_list_actions = ["s3:ListBucket", "s3:GetBucketLocation"]

sf_s3_rw_actions = [
  "s3:PutObject",
  "s3:GetObject",
  "s3:GetObjectVersion",
  "s3:DeleteObject",
  "s3:DeleteObjectVersion"
]

sf_s3_kms_actions = [
  "kms:Decrypt",
  "kms:Encrypt",
  "kms:GenerateDataKey*",
  "kms:ReEncryptTo",
  "kms:ReEncryptFrom",
  "kms:DescribeKey"
]

user_workspaces = {
  ws_type = "u"
  root_prefix = "home"
  s3_prefixes = [
    "AROAUHJVHXJWSHYYCJQ2W:X_BZDD56",
    "AROAUHJVHXJWSHYYCJQ2W:X_LZDB6T",
    "AROAUHJVHXJWSHYYCJQ2W:X_RZ5YV3",
    "AROAUHJVHXJWSHYYCJQ2W:X_BZZYHB"
  ]
  sf_external_id = {
    dev = ["ALLY_SFCRole=5603_9vvrJ7HZTPaq4HTtYy2w8UK/xXA="]
    qa = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw=q"]
    prod = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw=p"]
  }
}

team_workspaces = {
  ws_type = "t"
  root_prefix = "team"
  team_configs = {
    TECH-PLTFRM-AWS = {
      name = "TECH-PLTFRM-AWS"
      env_scope = {
        dev = ["innov", "test"]
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_PLATFORM_AWS", "CUMULUS"]
      sf_ro_external_id = {
        dev = ["ALLY_SFCRole=5603_ouUKf1C/r0ofID5VtuQ8LVqBSIQ="]
        qa = ["ALLY_SFCRole=5603_ovz1bhZv6z0ilo7+z3gJJeEnk2U="]
        prod = ["ALLY_SFCRole=5603_rIWflOvihKKnuqvrPXnVEbzzDc0="]
      }
      sf_rw_external_id = {
        dev = ["ALLY_SFCRole=5603_WBV3pdzGd5smyyyG1X3m8cJU3mY="]
        qa = ["ALLY_SFCRole=5603_DElk/1yrMj0s+C+wmniMMXVoiGY="]
        prod = ["ALLY_SFCRole=5603_KQ7IwRLd3v2+ZpnVBf7/S6Xc7rc="]
      }
    }
    AUTO-BUS-OPTI = {
      name = "AUTO-BUS-OPTI"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_BUSINESS_OPTIMIZATION"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_D4z7hidprXDEFNKKNNhBFaejTL8="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_RAwG/+EgVMacnYmJmyz3MHJ7iFs="]
      }
    }
    AUTO-DSS = {
      name = "AUTO-DSS"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS"]
      sf_ro_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = ["ALLY_SFCRole=5603_JUPre8DJwun6ylHRumUzzMTjS08="]
        prod = ["ALLY_SFCRole=5603_6hZQJ3xl9Kh4sDv5tsNiRyf1PGw="]
      }
      sf_rw_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = ["ALLY_SFCRole=5603_jaCDcNUOekV3LFofN014khqiXZ0="]
        prod = ["ALLY_SFCRole=5603_2EBYwPZmxYLzEMcf9e+BY30Ew28="]
      }
    }
    AUTO-DSS-CS-LP = {
      name = "AUTO-DSS-CS-LP"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS_CAM_CS_LP"]
      sf_ro_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = []
        prod = ["ALLY_SFCRole=5603_/0Xz4piFl0KiGQuYbjLrv0yOjqY="]
      }
      sf_rw_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = []
        prod = ["ALLY_SFCRole=5603_CuJp/oCjfT9jFxLt9m0rNvrgro8="]
      }
    }
    AUTO-DSS-DIG = {
      name = "AUTO-DSS-DIG"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS_DIGITAL"]
      sf_ro_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = [
          "ALLY_SFCRole=5603_oW5Lsu0+s3/Q6Dtc3GKK62FGAPc=",
          "ALLY_SFCRole=5603_rPLwhGjKckR6rPhppw0xNXrSQr4=",
          "ALLY_SFCRole=5603_2KsFSps5VWSgud0LhwODm0TZmYg=",
          "ALLY_SFCRole=5603_8U8IWWe1fDcCupp/UgKJtM8RMC8="
        ]
        prod = ["ALLY_SFCRole=5603_XonTS21drz5XnpASvr3D4sHlXOQ="]
      }
      sf_rw_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = [
          "ALLY_SFCRole=5603_qrIOaLD0kbgHNS5rhMBn1jdtOMA=",
          "ALLY_SFCRole=5603_bT9hgzgKe6cznd++mUaIY/9B314=",
          "ALLY_SFCRole=5603_Jfh0nAZd9YIKM6uZ4UmQQU8kIpQ=",
          "ALLY_SFCRole=5603_00GWOInk2hxPXjHzcUpfBh4u+cA="
        ]
        prod = ["ALLY_SFCRole=5603_GUBQwP5MOY9dBe4hy5DmYmXXNQA="]
      }
    }
    AUTO-INSU-SPA = {
      name = "AUTO-INSU-SPA"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_INSURANCE_SPA"]
      sf_ro_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = []
        prod = ["ALLY_SFCRole=5603_hK1VeeX2B8hlDok2hrfdx+vmkhA="]
      }
      sf_rw_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = []
        prod = ["ALLY_SFCRole=5603_q/MakwrWLMs0E/dJv45rPal/ubk="]
      }
    }
    TECH-CIH = {
      name = "TECH-CIH"
      env_scope = {
        dev = []
        qa = ["dev", "sit"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_CIH"]
      sf_ro_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = ["ALLY_SFCRole=5603_EP9w1+rR/McKkqye3/hauH/Q7Dc=", "ALLY_SFCRole=5603_vXhakboTQMGZ4msBN+P31+MdAIY="]
        prod = ["ALLY_SFCRole=5603_RanWC2rr3hf1+NKSILzqfuw/Aig="]
      }
      sf_rw_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = ["ALLY_SFCRole=5603_RSB5MHXjDVI5iUZZstn7GNYGPWM=", "ALLY_SFCRole=5603_IvC5rh9Q3+KBcKj7pAsudRXlZSQ="]
        prod = ["ALLY_SFCRole=5603_IVe1RpmOqPt9LngTyYytn7Hh0c4="]
      }
    }
    TECH-QMA = {
      name = "TECH-QMA"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_QMA"]
      sf_ro_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = []
        prod = ["ALLY_SFCRole=5603_MPBUuFZxe2IjnedxSgjuBdiuxh4="]
      }
      sf_rw_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = []
        prod = ["ALLY_SFCRole=5603_O/3xEmrYGJr+SxU+0RqqqFwhaUk="]
      }
    }
    TECH-MEAD = {
      name = "TECH-MEAD"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_MEAD"]
      sf_ro_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = ["ALLY_SFCRole=5603_dC+kJmnG+38/obnioafmWkwsWDE="]
        prod = ["ALLY_SFCRole=5603_ai/EJ+ntxumKeH/BReW0Tgwk48s="]
      }
      sf_rw_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = ["ALLY_SFCRole=5603_5vwmaFm7nXkOwPJtfm0TdCm/cx8="]
        prod = ["ALLY_SFCRole=5603_z5JcPq3eMiFW1WDc3LYcY2RkUrg="]
      }
    }
    TECH-PLTFRM-NUC = {
      name = "TECH-PLTFRM-NUC"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_PLATFORM_NUCLEUS"]
      sf_ro_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = ["ALLY_SFCRole=5603_LNMPeQMR3JAmr9Zpxt05JQIVGuU="]
        prod = ["ALLY_SFCRole=5603_Gq77ABRuX5MzVnlQJc7xk9i9pto="]
      }
      sf_rw_external_id = {
        dev = ["ALLY_SFCRole=2762_mmnMj+O6lsTAi8TwngUlsmWU3rw="]
        qa = ["ALLY_SFCRole=5603_wYvCWUW+qMjE2w41dfH5J23nKxM="]
        prod = ["ALLY_SFCRole=5603_C8VWMwlw3HmJDyQgPsLxKPKwHb4="]
      }
    }
    ATA-BTCMP = {
      name = "ATA-BTCMP"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = []
      }
      s3_prefixes = ["ATA_BOOTCAMP"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_PyUlWwBGa9U1t59A68OHu+H/7ZA="]
        prod = []
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_aXtARlxgfITUhpNaoVkrxISLHOA="]
        prod = []
      }
    }
    AUDT-SER = {
      name = "AUDT-SER"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUDIT_SERVICES"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_BfdWtkhnmDlyLjtibEyj6bzkjkw="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_+0pmRhxVtnhC8tIpw5ecva1eYfU="]
      }
    }
    AUTO-ADV-OPTI = {
      name = "AUTO-ADV-OPTI"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_ADVANCED_PROCESS_OPTIMIZATION"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_RPy/6rnGd0WpNSLOioK4F2Yqzsk="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_387EFdJk6XeqaKdZR+ZGvaFj954="]
      }
    }
    AUTO-AUTOVAL = {
      name = "AUTO-AUTOVAL"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_AUTOVAL"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_ng7y8KvrK4sdvXySruT64JNCcUI="]
        prod = ["ALLY_SFCRole=5603_kQ7lCz9FJ2ehM0fsAG4OSjewVW4="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_BZFlX8OLlDcdfNHGqEXG//uKClk="]
        prod = ["ALLY_SFCRole=5603_91xdreErQofXMDrKi1v4DX006v4="]
      }
    }
    AUTO-CAM-STR = {
      name = "AUTO-CAM-STR"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_CAM_STRATEGY"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_UcTKVDSICNuNEzzajAXqAh7Thn8="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_X7uuCI1jBZUWlDgV435zQKSXkDs="]
      }
    }
    AUTO-CL-BLCR = {
      name = "AUTO-CL-BLCR"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_CL_BLCR"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_tQ3lmqOVZkFc4gunt96QPptHev4="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_HO3y6kGozIQAYafJEV8+35rkN1I="]
      }
    }
    AUTO-COSA = {
      name = "AUTO-COSA"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_COSA"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_va/HiB4UubbtSXgmVc1Eh5YF7Z0="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_11PuKn6j0jY2u9euOVKD9SPFXmc="]
      }
    }
    AUTO-COSA-AA = {
      name = "AUTO-COSA-AA"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_COSA_AA"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_7whbTYJ4DEvPNzadY0IkYsIw8Wk="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_EhtTGmgO4YTKXmvnv9R27TNLQrM="]
      }
    }
    AUTO-COSA-ADS = {
      name = "AUTO-COSA-ADS"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_COSA_ADS"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_PBno1JHw2HUkzLeZlh63AoNEXEY="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_tPeUFQy92flq1CLxuEvZkT+XVbg="]
      }
    }
    AUTO-COSA-CCO = {
      name = "AUTO-COSA-CCO"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_COSA_CCO"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_6mn/TnV86T65TNZ9ffEdKGoHwgQ="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_54hxNMlC0EgJ7lA2so8GSKTw5D4="]
      }
    }
    AUTO-COSA-CP = {
      name = "AUTO-COSA-CP"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_COSA_CP"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_iwXu8UUfFWvk7TeUTItJIwc64vs="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_FXEOdMzi255fAlH4XMMVioJzdNA="]
      }
    }
    AUTO-COSA-NDLDG = {
      name = "AUTO-COSA-NDLDG"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_COSA_NDLDG"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_96ObIXdfAh5UuiEcfEFZCGznDs4="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_nZllxU2H1R772IkC8l+oT8288/4="]
      }
    }
    AUTO-COSA-PRM = {
      name = "AUTO-COSA-PRM"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_COSA_PRM"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_1LcNsDCMC7kg+xflQcg19j0phq0="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_GuSlv2MOmgsiW/Xjm80LieFl0Hw="]
      }
    }
    AUTO-COSA-SASO = {
      name = "AUTO-COSA-SASO"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_COSA_SASO"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_ieY57G725ybSLvQy8RRyxOMeF3I="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_S7hblDz9mW7jgdnpt5ik4Yl6+X4="]
      }
    }
    AUTO-CRD-OPS = {
      name = "AUTO-CRD-OPS"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_CREDIT_OPS"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_FMAn1JwkynrxRQUEsDsk7s1P7qc="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_yPGnhubLRTXBllNZtfA2E72lxto="]
      }
    }
    AUTO-DIR = {
      name = "AUTO-DIR"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DIRECT"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_TLj+qvAmzRK8dXMkkdH6ZqE2WpY="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_Tu5YlFqLcvR6aogVr/eAorlL2HU="]
      }
    }
    AUTO-DLR-MGMT = {
      name = "AUTO-DLR-MGMT"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DLR_MGMT"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_X66AbpLKskPyXIcEOqMgzsLoGfY="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_W7yjRmI2i6WVioDOjCV9cVrwDIs="]
      }
    }
    AUTO-DSS-ARC-SSO = {
      name = "AUTO-DSS-ARC-SSO"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS_ARC_SSO"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_6Hf+dmSORhAqLIYnvzVq4IHf03w=",
          "ALLY_SFCRole=5603_n4+H7lPjUvy1ieY/3PWx2eEHMjo=",
          "ALLY_SFCRole=5603_V61gWu/99ym8LmM3F6vGdIKXYuM="
        ]
        prod = ["ALLY_SFCRole=5603_ESLFusOJu6vmpf/Cf5AX6EK5FaQ="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_YV+UsZfRlKcPSAAa5jXzHlyl9Zw=",
          "ALLY_SFCRole=5603_xDbXJHdj8BLOfWXLdW4BJbr77ak=",
          "ALLY_SFCRole=5603_18WVZqPGIUm6Sz52f1VvlwKlIPo="
        ]
        prod = ["ALLY_SFCRole=5603_eoA+EI2d+PzxaCiBR6d/4FW0uNE="]
      }
    }
    AUTO-DSS-CON-STR = {
      name = "AUTO-DSS-CON-STR"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS_CONS_STRATEGY"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_fNnf5WU8G/4qGPIDf1HFvf19ykA=",
          "ALLY_SFCRole=5603_8ljnMYMdmaTTnvligjY5yXm75mY=",
          "ALLY_SFCRole=5603_FGFKma11hH8Uc2A/BMyQEs3U8zM=",
          "ALLY_SFCRole=5603_0kCFHbsmZ59sMECIrOQkBccPiK0="
        ]
        prod = ["ALLY_SFCRole=5603_1jzM/0lh4oYG2fnsqm3wOo0bhsA="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_n4+H7lPjUvy1ieY/3PWx2eEHMjo=",
          "ALLY_SFCRole=5603_CK4KxI9CiZnajfMEs/X+1UbpriY=",
          "ALLY_SFCRole=5603_us13RgNTITWZiwkpj804myiN/4g=",
          "ALLY_SFCRole=5603_x0RJUqKMvoSbVsQUqboWUZmRLQk="
        ]
        prod = ["ALLY_SFCRole=5603_nty4ISQqtct1KUvWMEWvCE0LEBM="]
      }
    }
    AUTO-DSS-DA = {
      name = "AUTO-DSS-DA"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS_DA"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_5/rmoaSkn0o1yIn0E4WtLkFIwfs="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_eXfwHSpSWs8q2ulWWagsVgphKqw="]
      }
    }
    AUTO-DSS-DIR = {
      name = "AUTO-DSS-DIR"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS_DIRECT"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_OS9xzeis0cww+Ds4TOAtd5yh8Bo="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_AOU0VJnfEwWK2OCE7frIMwsyXDg="]
      }
    }
    AUTO-DSS-ORIG = {
      name = "AUTO-DSS-ORIG"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS_ORIG"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_2AetK2CRgev8OclC5F8DZBTKA1w="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_OPqf8g/acBRDAxmvT1qdr5RiStQ="]
      }
    }
    AUTO-DSS-SLS = {
      name = "AUTO-DSS-SLS"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS_SLS"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_es+XlRrjX8rXO63fa+lq0GMXMU0="]
        prod = ["ALLY_SFCRole=5603_gZXkcgkvgv1QrLjGDf53EXC5CKM="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_FwR2Lj1KoZ3agtuLOcCo0eOqC58="]
        prod = ["ALLY_SFCRole=5603_SMO2i8alj1Ds8Q47qstvYt5BwEQ="]
      }
    }
    AUTO-DSS-UW = {
      name = "AUTO-DSS-UW"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_DSS_UW"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_ts4yjab6F3x9iFYBpkV0eC7hXPM="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_+LAnplPgP3qMh4hsQBQlh4R2eNA="]
      }
    }
    AUTO-ICP = {
      name = "AUTO-ICP"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_ICP"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_9vuXCKnEn6IL7EGpbMDREPctAbI="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_KqMzVYUgCYyKZjQzpDWw3ZySfpE="]
      }
    }
    AUTO-PRTFL-RSK = {
      name = "AUTO-PRTFL-RSK"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_PORTFOLIO_RISK"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_MajL5l/vavNvCItNnzvyG7RekIk="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_misPxYntCB7ic4+4DH9BhQBzk9Q="]
      }
    }
    AUTO-PRC = {
      name = "AUTO-PRC"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_PRICING"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_4BshF+oH8MCyY/yJ7noolwi2VtY="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_Qjm3CMy/FeHBQLKcojB5eTJ56Fg="]
      }
    }
    AUTO-RMRKT-3PR = {
      name = "AUTO-RMRKT-3PR"
      env_scope = {
        dev = []
        qa = ["model"]
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_REMARKETING_3PR"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_JfpH0iZpNPyDTpvi0AIIh5fPOZ0="]
        prod = ["ALLY_SFCRole=5603_H65WA09GXQa6PWnxXe6EHhvwv/E="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_Z2WzQ0YU1cACrLMFZZ5fcGN0fmk="]
        prod = ["ALLY_SFCRole=5603_gRyljZ25F9mp0soRkkdfHkd8ObM="]
      }
    }
    AUTO-RSK-FRCST = {
      name = "AUTO-RSK-FRCST"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_RISK_FORECAST"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_OiwL9yC7aES0h8CmoqaGm1Q55Bo="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_XUXWXTXULSr4UqY4txevOv4fT1E="]
      }
    }
    AUTO-SLSALLNCE = {
      name = "AUTO-SLSALLNCE"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_SALESALLIANCE"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_dJusTg6muqvTICmDlZCFBycpNBU="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_7OdaXxmWkPjxrIj3eNjIRC3+idQ="]
      }
    }
    AUTO-SP-ADR = {
      name = "AUTO-SP-ADR"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_SP_ADR"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_Q8K3NSHnSpiMPQPFT+HfQEWX1Z0="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_CZKrMp21TWTMV1t2hQ6rYScSWs0="]
      }
    }
    AUTO-SP-ICP = {
      name = "AUTO-SP-ICP"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["AUTO_SP_ICP"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_9vuXCKnEn6IL7EGpbMDREPctAbI="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_KqMzVYUgCYyKZjQzpDWw3ZySfpE="]
      }
    }
    BANK-CCERA = {
      name = "BANK-CCERA"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["BANK_CCERA"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_Kptrgrg9pIfMpe2B0RlsYH+0LCE="]
        prod = ["ALLY_SFCRole=5603_w3jrs7xMIVap8oh7GCMVNBDp3Uk="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_5816k5n6dUUHPHk9I1ZsgZMXNUs="]
        prod = ["ALLY_SFCRole=5603_NpDM7Dgdd9RrTjiWenMTWYKH1DI="]
      }
    }
    BANK-CON = {
      name = "BANK-CON"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["BANK_CONSUMER"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_SqcZC7sk7+Rdp3dQFxkM9/kqrrY="]
        prod = ["ALLY_SFCRole=5603_98RtkGbXE74ZSa6ry7s7aIfeTmE="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_YsAb2Ho66emfp8EG5s6MQzwNvH8="]
        prod = ["ALLY_SFCRole=5603_0xdrLxO8duMsGiX7Eoc6bSse3QM="]
      }
    }
    BANK-CX-CC = {
      name = "BANK-CX-CC"
      env_scope = {
        dev = []
        qa = ["model"]
        prod = ["prod"]
      }
      s3_prefixes = ["BANK_CX_CC"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_R42bFAH+nXsCDDg6Iu7xn+yE+Z8="]
        prod = ["ALLY_SFCRole=5603_uQeyk1xlsQTYJe0s8IaV/PVOnpQ="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_pGLaqvUPg3HXB59sPL5XxiS8o9E="]
        prod = ["ALLY_SFCRole=5603_JlkQdFMs9tMb7fmbRBOXdIQpOLg="]
      }
    }
    BANK-DEP-OPS = {
      name = "BANK-DEP-OPS"
      env_scope = {
        dev = []
        qa = ["model"]
        prod = ["prod"]
      }
      s3_prefixes = ["BANK_DEPOSIT_OPS"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_6XCIqB1ns44nBsgDv4Bd+06fv70="]
        prod = ["ALLY_SFCRole=5603_rYt59TIb1iaKScSM6pIdZjTNrM8="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_rEPUT4hmZMhAOa87/Qj7nRwG8w4="]
        prod = ["ALLY_SFCRole=5603_tu3Fbx74qmf14gTvY/uFRuPVozE="]
      }
    }
    BANK-DSS-DEPINV = {
      name = "BANK-DSS-DEPINV"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["BANK_DSS_DEPINV"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_45LytMVug50pckCv5q7MRlzqtMc=",
          "ALLY_SFCRole=5603_J0e/ibrMvnzDD6F8QGMCXFQZpxQ=",
          "ALLY_SFCRole=5603_7NLURALaqw2wDHvn8m4OIkfQ5KI=",
          "ALLY_SFCRole=5603_FtNoKuzpbrTPNtChRonhCB0xZWo="
        ]
        prod = ["ALLY_SFCRole=5603_MZlm4rWJsxXTVu2FsZeQIxPHEIs="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_FZgSYG34Pxri6PCbWZU/ax+5XRI=",
          "ALLY_SFCRole=5603_0Ma8Y/DNTQQrrhrWvdPSuMgLj5E=",
          "ALLY_SFCRole=5603_7NLURALaqw2wDHvn8m4OIkfQ5KI=",
          "ALLY_SFCRole=5603_zMFtYyTx073pMePhnMt3ruPk1oM="
        ]
        prod = ["ALLY_SFCRole=5603_Nd0TqD/eRvvnIPZ3axK3qwF5mig="]
      }
    }
    BANK-FRD-STR = {
      name = "BANK-FRD-STR"
      env_scope = {
        dev = []
        qa = ["model"]
        prod = ["prod"]
      }
      s3_prefixes = ["BANK_FRAUD_STRATEGY"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_+uskY3zxLaEb7CCETmMctUUS7TU="]
        prod = ["ALLY_SFCRole=5603_b6OJARytS4Q4NmfXMO/ATHYG1tA="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_j7HDtMWWac6He5uF9VLyerIs+mY="]
        prod = ["ALLY_SFCRole=5603_tTuglOYKVtWnfOSEgrNUy33VRhw="]
      }
    }
    BANK-INV = {
      name = "BANK-INV"
      env_scope = {
        dev = []
        qa = ["model"]
        prod = ["prod"]
      }
      s3_prefixes = ["BANK_INVEST"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_2dtC/baxwGdR2pshFCQhwY+Gkuk="]
        prod = ["ALLY_SFCRole=5603_/3rMUjLdI0cQhgq0XDAHHOWc5Mg="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_K42lFc1wP/alOuEAahVe1UBYYwQ="]
        prod = ["ALLY_SFCRole=5603_0YCfG2UvJIhfjhkClw9q/uLuLpk="]
      }
    }
    BANK-MORT-BA = {
      name = "BANK-MORT-BA"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["BANK_MORT_BA"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_2dXzypBFDiCu6i9s5TMtOgClWoQ="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_glXdFl48Fkq44PEl4vVJTUV1HjA="]
      }
    }
    COMP-AML = {
      name = "COMP-AML"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["COMPLIANCE_AML"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_63rxTzyG91CapXYtovzHJnFZgmg=",
          "ALLY_SFCRole=5603_GKXqMsbnSAm1cOOXkuPNgEz1MZc=",
          "ALLY_SFCRole=5603_D1yGoItfB9Cu6mIow+WoNvjRazE=",
          "ALLY_SFCRole=5603_ROQs5JPaFqm0hLbql85rLztwDY8="
        ]
        prod = ["ALLY_SFCRole=5603_Kzc6Sp/vwCNy6fox/n79bRxCnY8="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_PuZxUB8/atJlXb3XIdHRgpulr3c=",
          "ALLY_SFCRole=5603_rMdKhjEADCsgIZrPEIa+Gs/07xQ=",
          "ALLY_SFCRole=5603_RdhNsRqcHMgPhfC2A/Rz5rSjXTY=",
          "ALLY_SFCRole=5603_ROQs5JPaFqm0hLbql85rLztwDY8="
        ]
        prod = ["ALLY_SFCRole=5603_zQ/VyHhnF3Imp93qHJsFOmao2fg="]
      }
    }
    COMP-EFSI = {
      name = "COMP-EFSI"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["COMPLIANCE_EFSI"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_WA+lmnVbH2xOozUtC3ODkF3PWCE=",
          "ALLY_SFCRole=5603_Fx17Qh1Gk+iWyE7+nZb8Dr35gms=",
          "ALLY_SFCRole=5603_d+sypLmc94u7XSmi2D5gDicv8gc=",
          "ALLY_SFCRole=5603_uQ4AEF66go0MBepP9XP5CVxWwBk="
        ]
        prod = ["ALLY_SFCRole=5603_78Hc2sSkvKlpZZfHlrAeM8MyMNI="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_f39lQUT6CLswL1kpwYo5DNMMopc=",
          "ALLY_SFCRole=5603_IIiq98ncKr5uLvv9AqrjZ7NF6LM=",
          "ALLY_SFCRole=5603_SgpSCVcwPbDXPt2zlAiUjKWZxN4=",
          "ALLY_SFCRole=5603_OyP53SUkD/EZp5FXw2ibn3oJcFw="
        ]
        prod = ["ALLY_SFCRole=5603_1DbXp53l7yTjbJELp7LYoykbMds="]
      }
    }
    FIN-ACC-ENG = {
      name = "FIN-ACC-ENG"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["FINANCE_ACCOUNTING_ENG"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_61VuplklsnZ8zs8JWw6Pyy3hYvo=",
          "ALLY_SFCRole=5603_qxx+a0IyLovQs+/qUFvVcVWuABM=",
          "ALLY_SFCRole=5603_dwvfoOROpPTrzMJ0MtNnzeLWuUA=",
          "ALLY_SFCRole=5603_giuvsUUxDe9C2hV62Os/QCtH4Og="
        ]
        prod = ["ALLY_SFCRole=5603_L7Ld8s3eTsC9XExtjNs8N7ODblo="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_gwLlRCJAN18nFjTd8NL1FF4aW/M=",
          "ALLY_SFCRole=5603_JhRHmQCL/0UvJ4PtwjxHFhJIlTg=",
          "ALLY_SFCRole=5603_trXR1VJ0ItGlk72jHo9hX4b4h6s=",
          "ALLY_SFCRole=5603_JDa0yoqdyIscqWq0TAdDQ5GznR8="
        ]
        prod = ["ALLY_SFCRole=5603_DmpUv5G/b/p1w/NW38eKii3xo/4="]
      }
    }
    FIN-AUTO-CFO = {
      name = "FIN-AUTO-CFO"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["FINANCE_AUTO_CFO"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_To9GdbJvGfLxjSMgkjgRHl01RiQ="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_lsL9+SXr4+v7/gAQ6GwVUaPd0Wo="]
      }
    }
    FIN-CPTL-MRKTS = {
      name = "FIN-CPTL-MRKTS"
      env_scope = {
        dev = []
        qa = ["dev", "model"]
        prod = ["prod"]
      }
      s3_prefixes = ["FINANCE_CPTL_MARKETS"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_bvMbGrOdFvlwe2VcrQY3ILCSCww=", "ALLY_SFCRole=5603_2oJcHWbktA57cLIwDKx5KNxUCuk="]
        prod = ["ALLY_SFCRole=5603_MJjehjOw0VwFoSXuLj/h/Tw4Tg0="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_/PevDlN5qsd/C/GXAwjGSVFBhZg=", "ALLY_SFCRole=5603_qOwCGRvKP7OpwCoxpnrsJjdd6aY="]
        prod = ["ALLY_SFCRole=5603_u0IxNREEdCLjrAxAxorGWG39SBE="]
      }
    }
    FIN-ECNMCS = {
      name = "FIN-ECNMCS"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["FINANCE_ECONOMICS"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_cKO/HEnIFtxuVVUTefDWS1Y5cd8="]
        prod = ["ALLY_SFCRole=5603_s4NqS8+05izGrtmzJvK/JbYzfks="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_PCEWZ/Z3zscY8dSPPcYPbvD8AQU="]
        prod = ["ALLY_SFCRole=5603_T4Y8AWeGUsdTlaXxJZt5K/Q/6i0="]
      }
    }
    FIN-FMA-MDL = {
      name = "FIN-FMA-MDL"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["FINANCE_FMA_MODEL"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_FyZPeL64gefPfAvv3nsBYjgkq/Q="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_6PZspNp9Y/Atm9xUsXbRrkthR3I="]
      }
    }
    FIN-GL-SAP = {
      name = "FIN-GL-SAP"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["FINANCE_GL_SAP"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_xpjSYeBXN8c8eV5lMs88zs3CdtY=",
          "ALLY_SFCRole=5603_1VgAlAQCrp3X3qML05EmDnuRUqY=",
          "ALLY_SFCRole=5603_vh2RE0zUbvFjI4Ce8mB+7C7G+V8=",
          "ALLY_SFCRole=5603_j4xM6KFKy0Az1B3JnFHKSuAC5/o="
        ]
        prod = ["ALLY_SFCRole=5603_lxq1ovRGdsN/SiH5fQIRdYhlstU="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_rKA9j1leDE25uvGaIUs6MB/2uVw=",
          "ALLY_SFCRole=5603_7VetuLQOBlR8yGlgFenEit8eiqI=",
          "ALLY_SFCRole=5603_pphKphrsU9mwn+I++K0yejNVabc=",
          "ALLY_SFCRole=5603_6nHgIqyv4PCDeBGooELth9SUKNY="
        ]
        prod = ["ALLY_SFCRole=5603_lQxbIzogM9PK2vs90daSHEeI1j8="]
      }
    }
    FIN-SPLY-CHN = {
      name = "FIN-SPLY-CHN"
      env_scope = {
        dev = []
        qa = ["dev", "model"]
        prod = ["prod"]
      }
      s3_prefixes = ["FINANCE_SUPPLY_CHAIN"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_xQgvMXMMnswGvGzlIFHjfKnEaoo=", "ALLY_SFCRole=5603_4PnPDsDiCrTyctditQbZidW4msw="]
        prod = ["ALLY_SFCRole=5603_j2Qjpa7cJiqYdFbXSeXkgzT6o+8="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_gfy3x6Bd4kaTMS4DXLfSPtBUiqU=", "ALLY_SFCRole=5603_u1IWubXlzh4XoCtRXSMOZeDRJn8="]
        prod = ["ALLY_SFCRole=5603_5GIYhPvi6XvtldsetJzvU+7Adzo="]
      }
    }
    FIN-VAR-LIB = {
      name = "FIN-VAR-LIB"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["FINANCE_VARIABLES_LIBRARY"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_s2QrQ9hMTRiyImwCz7K2oirBX/Q=",
          "ALLY_SFCRole=5603_FAOF0YgM934Z8tcjVd6Me6kL3NI=",
          "ALLY_SFCRole=5603_wqRDc1TefwGDQWisddzs0vduv/E=",
          "ALLY_SFCRole=5603_06qA6cX00pCjhClcpYFJFaJwUDU="
        ]
        prod = ["ALLY_SFCRole=5603_f85Gz3O4uICVTKRkhfHcN+KbuLk="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_ZiXSdI4D7GFF/FaiaRlO8ddiuws=",
          "ALLY_SFCRole=5603_coMfFLC3wKP2U6pBKce216wYc5k=",
          "ALLY_SFCRole=5603_oAae01jYKVyh/kkb5LNA6Zfbdq0=",
          "ALLY_SFCRole=5603_sLai+prK1ePifuSrrhhfX70xYrA="
        ]
        prod = ["ALLY_SFCRole=5603_Zd/b8MRrOUkKE9IR5sY9piiiofo="]
      }
    }
    HR-PEP-SYS = {
      name = "HR-PEP-SYS"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["HR_PEOPLE_SYSTEMS"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_sxholPedZh+pPO9Tz1sDJ+seKNo="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_V6bCUun/sIddeYFRF2mQSHhZSFM="]
      }
    }
    HR-WRKFRC = {
      name = "HR-WRKFRC"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["HR_WORKFORCE"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_1Z5eBBclPJ2pvC4f71nPwxVZoAo="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_5OOmLGXwLCsCnVViNOPOPeXReu0="]
      }
    }
    INS-DWR = {
      name = "INS-DWR"
      env_scope = {
        dev = []
        qa = ["model"]
        prod = ["prod"]
      }
      s3_prefixes = ["INSURANCE_DWR"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_AVJPylKDOQbeJrDZTbL3nf7+xqE="]
        prod = ["ALLY_SFCRole=5603_26yXxrp+2YRQmZwimfOAx/vC7i8="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_F14t65xtEYNqul5TwdIQMYMUKCc="]
        prod = ["ALLY_SFCRole=5603_6g70mQpuWXeOQ/BPpFQmvfVIfmE="]
      }
    }
    IRM-AUTO-CRD = {
      name = "IRM-AUTO-CRD"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["IRM_AUTO_CREDIT"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_munECyPkKe6IkaPvQjfV2B722bI="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_tXjcQOWSLki6mdLjTbCa9WsYzYI="]
      }
    }
    IRM-CARD = {
      name = "IRM-CARD"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["IRM_CARD"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_XX9/Q2NbYGrVSv6QK/W0xplGbcg="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_Rn7Hu4vyyrIwM/rRiUs0ye+Npr4="]
      }
    }
    IRM-ESG = {
      name = "IRM-ESG"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["IRM_ESG"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_L59dgUVsUwceUnEUctT3XsqUpbA="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_Eec3tsyD6JBl1MBrsfyxwzc14DE="]
      }
    }
    IRM-INS-RSK = {
      name = "IRM-INS-RSK"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["IRM_INSURANCE_RISK"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_EThBk+/Q6XUm8sSYYAeTk53GB6o="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_NYaPISrPsUT10R8EMdlkNAFFJ+k="]
      }
    }
    MKTG-CRM = {
      name = "MKTG-CRM"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["MARKETING_CRM"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_rObyn5KYaxhC9uvhkF1JSCealtE="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_ol13wpPEAPsxiBIl0vu4TwROVAM="]
      }
    }
    MKTG-DIG = {
      name = "MKTG-DIG"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["MARKETING_DIGITAL"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_M49ijI2xSlaHo9ECqSvZIJoFn6E="]
        prod = ["ALLY_SFCRole=5603_ctqb47uFC7T2kzBs2BKTfCYoboA="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_cmdfKiYDJ37YQn/DkHS/1Q+qhfc="]
        prod = ["ALLY_SFCRole=5603_ncecvmgXqQagDE/HGZM1FxSTd9U="]
      }
    }
    TECH-ADV = {
      name = "TECH-ADV"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_ADVANTAGE"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_S6/jdwLvJFOHG1Lu0El0zxa+PMA="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_zMFJVHlLSdYaOW7JGsEiLCMn4Zk="]
      }
    }
    TECH-COMP-CRT = {
      name = "TECH-COMP-CRT"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = []
      }
      s3_prefixes = ["TECH_COMPLIANCE_CRT"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_yX0XCSnvuvD9lCkGCnEBfuVE3F0="]
        prod = []
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_961c/qIkZXowDE39s/qPKqW8wcE="]
        prod = []
      }
    }
    TECH-COMP-DENG = {
      name = "TECH-COMP-DENG"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_COMPLIANCE_DATAENG"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_mRD0ZQzyRHBWqwONkgKXX79HGjU=",
          "ALLY_SFCRole=5603_uD/++/Jlt4mPI6kkDPg2LyS0Z8g=",
          "ALLY_SFCRole=5603_y75pxbVGNu5q4YzRX1220BI7CFk=",
          "ALLY_SFCRole=5603_6Mw2ScYevAnA6l8VKERJ6VsIbzs="
        ]
        prod = ["ALLY_SFCRole=5603_Omz/B+4B2JsQ8wvBz7lYA5sjMMM="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_Q8rVITdETggEf5iSmIaJT/pZbtk=",
          "ALLY_SFCRole=5603_Duzpibgj0RTaZkGyj9htfaXJ/Nc=",
          "ALLY_SFCRole=5603_xzlQTYiG0kcKpDiMIyGSZIgGMK8=",
          "ALLY_SFCRole=5603_0gP4wu/YsK9BNlV9R6Kk6L6At3c="
        ]
        prod = ["ALLY_SFCRole=5603_X0A89ygYunfzPZCbPrCcM4sONNI="]
      }
    }
    TECH-COMP-FRD-CM = {
      name = "TECH-COMP-FRD-CM"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_COMPLIANCE_FRAUD_CASEMGMT"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_NHap0a9M4hlJcnrPzvP5TljzC9M=",
          "ALLY_SFCRole=5603_CyvssgP1L5kto2ja1dhWmIHXKsk=",
          "ALLY_SFCRole=5603_A+gQbsm+PNd3RJnyhrk6zM3TDyM=",
          "ALLY_SFCRole=5603_lxa2t67+MvB7AdsL1yxrYpnU9kQ="
        ]
        prod = ["ALLY_SFCRole=5603_7IMP54HPjF3IhNFQ3YYA+3sN5KE="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_1OW6YJZvyBgcw68UYougt6xl+xI=",
          "ALLY_SFCRole=5603_Bt/07V4sH+p6aDCj61wmV8E/N6Q=",
          "ALLY_SFCRole=5603_ZLGT/BqOhJIrEip8DU+Fya1dsnU=",
          "ALLY_SFCRole=5603_ZFqp8DcasADN7yjsCxEqOWFrCIc="
        ]
        prod = ["ALLY_SFCRole=5603_W2D7oouUMej2ss3zTYwKGr6fkJI="]
      }
    }
    TECH-COMP-SUS = {
      name = "TECH-COMP-SUS"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_COMPLIANCE_SUSTAIN"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_ekg24R1ZUsn4H0/zUU5c/YHd0nY=",
          "ALLY_SFCRole=5603_yS6rsZjc5EDMnhleFy6x2A2qWW4=",
          "ALLY_SFCRole=5603_RQxfMlnHX/b2QMO7Z7WnOPeYJhA=",
          "ALLY_SFCRole=5603_2UmKTC1ZX4lsQzW+rZ4uRrWH3Us="
        ]
        prod = ["ALLY_SFCRole=5603_m897Mq7UMz69lUl0kMxhs2owWDs="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_8jDNJB/wZc4VdP1HZICY/TrFvzQ=",
          "ALLY_SFCRole=5603_H6atM3D7gbx5yvuA6L6I/GGhIJU=",
          "ALLY_SFCRole=5603_SwQITm5oDmJuqdeh8BPhfXwbZjw=",
          "ALLY_SFCRole=5603_XJYW2JlFvVY3VPbFzCD6k2+Dm3Y="
        ]
        prod = ["ALLY_SFCRole=5603_4Jm93IsvAXP+NebGlGkym5s7Rwo="]
      }
    }
    TECH-CONVOAI = {
      name = "TECH-CONVOAI"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_CONVOAI"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_wfcE6PqBWKR2+ee3fMceyWh7hn8="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_cyIx6IWAs+bOAkdsO9oIxONkpL0="]
      }
    }
    TECH-DGO = {
      name = "TECH-DGO"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_DGO"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_hqsuchb8/6gwCLS4p9DTeLx9FCU="]
        prod = ["ALLY_SFCRole=5603_5DrHwUFzpQ9enybJBAeSRsZ3uSE="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_rAN3Oegu/pSNHlbUPLv+GeD64II="]
        prod = ["ALLY_SFCRole=5603_tQFd54iwSsxrNE1b8CCPORc7vB0="]
      }
    }
    TECH-EA-DATA = {
      name = "TECH-EA-DATA"
      env_scope = {
        dev = []
        qa = []
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_EA_DATA"]
      sf_ro_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_S/+9MNofdIIn0zZdqzjS3PwNtK4="]
      }
      sf_rw_external_id = {
        dev = []
        qa = []
        prod = ["ALLY_SFCRole=5603_4CW9/P+65qJbpAzY9pnlvTijCyw="]
      }
    }
    TECH-EDA-BD-SER = {
      name = "TECH-EDA-BD-SER"
      env_scope = {
        dev = []
        qa = [
          "dev",
          "sit",
          "model",
          "cap"
        ]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_EDA_BANK_DATA_SERVICES"]
      sf_ro_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_LhnMOwxl86ZPEjPD7Hkh9EPo2KQ=",
          "ALLY_SFCRole=5603_7gWjmw3eB2i0Hbe+DT/U/FauLgo=",
          "ALLY_SFCRole=5603_n8X5ns7xyhx3F1o+xQDPfeTRQt0=",
          "ALLY_SFCRole=5603_7ltrL4t9k2uYj1/nsSLtwsvJkTY="
        ]
        prod = ["ALLY_SFCRole=5603_HGxtQwZ9FWnSLit2KmvCNY+TbZA="]
      }
      sf_rw_external_id = {
        dev = []
        qa = [
          "ALLY_SFCRole=5603_dOpsMeKLBpAzLr3AQa4zc/JWur4=",
          "ALLY_SFCRole=5603_LCkG9xfDBlwEbMthJUz8xCBieVg=",
          "ALLY_SFCRole=5603_e7W6TNTzdSXQdeY+w56r7PT+wzQ=",
          "ALLY_SFCRole=5603_NLDd7E5whcvO8o4KRClBvBx3+3Q="
        ]
        prod = ["ALLY_SFCRole=5603_Eje3VPZOpT7wAfhxftjTU/o8RRc="]
      }
    }
    TECH-EDA-SOL-ENG = {
      name = "TECH-EDA-SOL-ENG"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_EDA_SOLUTION_ENG"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_XGW/FBh/ZuXIA+nfIwI5uiqIBZQ="]
        prod = ["ALLY_SFCRole=5603_oC/tjBQrmMlY4fXlQ/Kr2s8im3M="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_gsqwvHsdOiFH5vLrGo6mEmRib2A="]
        prod = ["ALLY_SFCRole=5603_ojhzFSaxzPp8g0elSkH00I/sLyo="]
      }
    }
    TECH-FUS = {
      name = "TECH-FUS"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_FUSION"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_y7dqtR+B7iDkevi+uUHFERw+rqE="]
        prod = ["ALLY_SFCRole=5603_Z8Efi4HnLq8TJQR9teYEG6xcBR8="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_MiMcauiLzA7Bdx/yqMCbe9W5Xx4="]
        prod = ["ALLY_SFCRole=5603_b5JevL5jxqo2GctSBnVliqHv3s0="]
      }
    }
    TECH-IPRM-CRSK = {
      name = "TECH-IPRM-CRSK"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = ["prod"]
      }
      s3_prefixes = ["TECH_IPRM_CYBERRISK"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_8ANl3t+dMDuggEV8y9v6/BZl3uY="]
        prod = ["ALLY_SFCRole=5603_ryuYdj40QRfbRE+frcy+Kf1Tj4I="]
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_bdcQSDrB4U5pcuLtAgZi7CfGy5E="]
        prod = ["ALLY_SFCRole=5603_cSevsgR7bAOw4B8oOEWX8J63Y4A="]
      }
    }
    TECH-SUS-EDA = {
      name = "TECH-SUS-EDA"
      env_scope = {
        dev = []
        qa = ["dev"]
        prod = []
      }
      s3_prefixes = ["TECH_SUSTAIN_EDA"]
      sf_ro_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_/Fp5dATtizACnEtGFBKdoXgksBU="]
        prod = []
      }
      sf_rw_external_id = {
        dev = []
        qa = ["ALLY_SFCRole=5603_wKfkjK6mu+UQYqphzxspdzvVVfI="]
        prod = []
      }
    }
  }
}
