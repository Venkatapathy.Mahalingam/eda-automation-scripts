#!/usr/bin/env python
# -*- coding: utf-8 -*-

from string import Template
import os, csv

# Function which initializes the variables needed for execution
def initialize_variables():
  global program_params

  program_params = {
    'app_name': 'eda',
    'service_name': 'tf-aws-analytics-s3',
    'bucket_purpose': 'workspaces',
    'ro_template_file_name': 'adfs-iam-role-analyst-ro-tf.tmpl',
    'rw_template_file_name': 'adfs-iam-role-analyst-rw-tf.tmpl',
    'params_file_name': 'adfs-iam-role-analyst.param',
    'env_abbr': {
      'D': {'env_type': 'nonprod', 'sdlc_name': 'dev', 'sdlc_workspace': 'qa-r-dev'},
      'S': {'env_type': 'nonprod', 'sdlc_name': 'sit', 'sdlc_workspace': 'qa-r-sit'},
      'M': {'env_type': 'nonprod', 'sdlc_name': 'model', 'sdlc_workspace': 'qa-r-model'},
      'C': {'env_type': 'nonprod', 'sdlc_name': 'cap', 'sdlc_workspace': 'qa-r-cap'},
      'P': {'env_type': 'prod', 'sdlc_name': 'prod', 'sdlc_workspace': 'prod'}
    }
  }


# Function which builds the AWS Resource ARNs for both S3 bucket and S3 prefix
def build_aws_arn(resource_type, resource_info):
  global program_params

  resource_arns = []
  if resource_type == 's3_bucket':
    for env_name in resource_info['env_scope']:
      resource_arns.append('      "arn:aws:s3:::' + program_params['app_name'] + '-' + 
        program_params['service_name'] + '-' +
        program_params['env_abbr'][env_name]['sdlc_workspace'] + '-' +
        resource_info['aws_region'] + '-' + program_params['bucket_purpose'] + '"')
  elif resource_type == 's3_object':
    for env_name in resource_info['env_scope']:
      for s3_prefix in resource_info['s3_prefixes']:
        # resource_arns.append('      "arn:aws:s3:::' + program_params['app_name'] +
        #   '-' + program_params['service_name'] + '-' +
        #   program_params['env_abbr'][env_name]['sdlc_workspace'] + '-' +
        #   resource_info['aws_region'] + '-' + program_params['bucket_purpose'] +
        #   '/team/TEAM_' + s3_prefix + '_' + env_name + '/"')
        resource_arns.append('      "arn:aws:s3:::' + program_params['app_name'] +
          '-' + program_params['service_name'] + '-' +
          program_params['env_abbr'][env_name]['sdlc_workspace'] + '-' +
          resource_info['aws_region'] + '-' + program_params['bucket_purpose'] +
          '/team/TEAM_' + s3_prefix + '_' + env_name + '/*"')
  return resource_arns


# Function which creates the Terraform file for RO Analyst IAM role
def create_ro_tf(input_params):
  global program_params

  env_names_list = input_params['env_names'].split(',')
  s3_prefixes_list = input_params['s3_prefixes'].split(',')

  resource_info = {'aws_region': input_params['aws_region'], 'env_scope': env_names_list}
  s3_bucket_arns = build_aws_arn('s3_bucket', resource_info)

  resource_info = {'aws_region': input_params['aws_region'], 'env_scope': env_names_list, 's3_prefixes': s3_prefixes_list}
  s3_bucket_prefix_arns = build_aws_arn('s3_object', resource_info)

  hiphenated_team_name = input_params['team_name'].replace('_', '-')

  template_values = {
    'hiphenated_team_name': hiphenated_team_name,
    's3_bucket_arn': "[\n{}\n    ]".format(',\n'.join(s3_bucket_arns)),
    's3_bucket_prefix_arn': "[\n{}\n    ]".format(',\n'.join(s3_bucket_prefix_arns)),
    'aws_account_id': input_params['aws_account_id'],
    'kms_key_id': input_params['kms_key_id'],
    'env_abbr': 'Prod' if input_params['env_names'] == 'P' else 'NonProd'
  }

  with open(program_params['ro_template_file_name']) as template_file:
    template_data = Template(template_file.read())

  output = template_data.substitute(template_values)

  output_dir = './' + input_params['aws_account_id']
  if not os.path.exists(output_dir):
    os.makedirs(output_dir)

  with open(output_dir + '/Ally-ADFS-T-' + hiphenated_team_name + '-Analyst-RO.tf', 'w') as outfile:
    outfile.write(output)


# Function which creates the Terraform file for RW Analyst IAM role
def create_rw_tf(input_params):
  global program_params

  env_names_list = input_params['env_names'].split(',')
  s3_prefixes_list = input_params['s3_prefixes'].split(',')

  resource_info = {'aws_region': input_params['aws_region'], 'env_scope': env_names_list}
  s3_bucket_arns = build_aws_arn('s3_bucket', resource_info)

  resource_info = {'aws_region': input_params['aws_region'], 'env_scope': env_names_list, 's3_prefixes': s3_prefixes_list}
  s3_bucket_prefix_arns = build_aws_arn('s3_object', resource_info)

  hiphenated_team_name = input_params['team_name'].replace('_', '-')

  template_values = {
    'hiphenated_team_name': hiphenated_team_name,
    's3_bucket_arn': "[\n{}\n    ]".format(',\n'.join(s3_bucket_arns)),
    's3_bucket_prefix_arn': "[\n{}\n    ]".format(',\n'.join(s3_bucket_prefix_arns)),
    'aws_account_id': input_params['aws_account_id'],
    'kms_key_id': input_params['kms_key_id'],
    'env_abbr': 'Prod' if input_params['env_names'] == 'P' else 'NonProd'
  }

  with open(program_params['rw_template_file_name']) as template_file:
    template_data = Template(template_file.read())

  output = template_data.substitute(template_values)

  output_dir = './' + input_params['aws_account_id']
  if not os.path.exists(output_dir):
    os.makedirs(output_dir)

  with open(output_dir + '/Ally-ADFS-T-' + hiphenated_team_name + '-Analyst-RW.tf', 'w') as outfile:
    outfile.write(output)


# Main function that is invoked first
def main():
  global program_params

  initialize_variables()

  input_list = []
  with open(program_params['params_file_name']) as input_file:
    for inline in csv.DictReader(input_file):
      input_list.append(inline)

  for item in input_list:
    create_ro_tf(item)
    create_rw_tf(item)


# Python program execution begins here
if __name__ == "__main__":
  main()
