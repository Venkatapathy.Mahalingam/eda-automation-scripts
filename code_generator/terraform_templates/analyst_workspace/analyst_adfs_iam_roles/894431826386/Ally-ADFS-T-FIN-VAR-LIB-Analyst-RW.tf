## Ally-ADFS-T-FIN-VAR-LIB-Analyst-RW
# Policies
##################################################################################################
data "aws_iam_policy_document" "Ally-ADFS-T-FIN-VAR-LIB-Analyst-RW" {
  statement {
    sid         = "S3LocateBucketAccess"
    effect      = "Allow"
    actions     = [
      "s3:ListAllMyBuckets",
      "s3:GetBucketLocation"
    ]
    resources   = ["*"]
  }

  statement {
    sid         = "S3ListAccess"
    effect      = "Allow"
    actions     = [
      "s3:ListBucket"
    ]
    resources   = [
      "arn:aws:s3:::eda-tf-aws-analytics-s3-qa-r-dev-us-east-1-workspaces",
      "arn:aws:s3:::eda-tf-aws-analytics-s3-qa-r-sit-us-east-1-workspaces",
      "arn:aws:s3:::eda-tf-aws-analytics-s3-qa-r-model-us-east-1-workspaces",
      "arn:aws:s3:::eda-tf-aws-analytics-s3-qa-r-cap-us-east-1-workspaces"
    ]
    condition {
      test      = "StringLike"
      variable  = "s3:prefix"
      values    = [
        "",
        "user/",
        "team/*"
      ]
    }   
  }

  statement {
    sid         = "S3WriteAccess"
    effect      = "Allow"
    actions     = [
      "s3:List*",
      "s3:Get*",
      "s3:Put*",
      "s3:DeleteObject"
    ]
    resources   = [
      "arn:aws:s3:::eda-tf-aws-analytics-s3-qa-r-dev-us-east-1-workspaces/team/TEAM_FINANCE_VARIABLES_LIBRARY_D/*",
      "arn:aws:s3:::eda-tf-aws-analytics-s3-qa-r-sit-us-east-1-workspaces/team/TEAM_FINANCE_VARIABLES_LIBRARY_S/*",
      "arn:aws:s3:::eda-tf-aws-analytics-s3-qa-r-model-us-east-1-workspaces/team/TEAM_FINANCE_VARIABLES_LIBRARY_M/*",
      "arn:aws:s3:::eda-tf-aws-analytics-s3-qa-r-cap-us-east-1-workspaces/team/TEAM_FINANCE_VARIABLES_LIBRARY_C/*"
    ]
  }

  statement {
    sid         = "S3KMSAccess"
    effect      = "Allow"
    actions     = [
      "kms:GetPublicKey",
      "kms:Decrypt",
      "kms:ListKeyPolicies",
      "kms:ListRetirableGrants",
      "kms:GetKeyPolicy",
      "kms:ListResourceTags",
      "kms:ReEncryptFrom",
      "kms:ListGrants",
      "kms:GetParametersForImport",
      "kms:Encrypt",
      "kms:GetKeyRotationStatus",
      "kms:GenerateDataKey",
      "kms:ReEncryptTo",
      "kms:DescribeKey"
    ]
    resources   = [
      "arn:aws:kms:us-east-1:894431826386:key/e1821f16-b8d3-471e-b01f-11bd3e85c8fe"
    ]
    condition {
      test      = "ForAllValues:StringEquals"
      variable  = "aws:TagKeys"
      values    = ["105211"]
    }
  }

  statement {
    sid         = "Support"
    effect      = "Allow"
    actions     = [
      "support:*"
    ]
    resources   = ["*"]
  }

  statement {
    sid         = "CloudWatchRO"
    effect      = "Allow"
    actions     = [
      "cloudwatch:DescribeAlarms",
      "cloudwatch:DescribeAlarmHistory",
      "cloudwatch:DescribeAlarmsForMetric",
      "cloudwatch:DescribeAnomalyDetectors",
      "cloudwatch:DescribeInsightRules",
      "cloudwatch:ListDashboards",
      "cloudwatch:ListManagedInsightRules",
      "cloudwatch:ListMetrics",
      "cloudwatch:ListMetricStreams",
      "cloudwatch:ListTagsForResource",
      "cloudwatch:GetDashboard",
      "cloudwatch:GetInsightRuleReport",
      "cloudwatch:GetMetricData",
      "cloudwatch:GetMetricStatistics",
      "cloudwatch:GetMetricStream",
      "cloudwatch:GetMetricWidgetImage"
    ]
    resources   = ["*"]
  }
}

resource "aws_iam_policy" "Ally-ADFS-T-FIN-VAR-LIB-Analyst-RW" {
  name        = "Ally-ADFS-T-FIN-VAR-LIB-Analyst-RW"
  description = "[Terraform] Ally-ADFS-T-FIN-VAR-LIB-Analyst-RW - NonProd"
  policy      = data.aws_iam_policy_document.Ally-ADFS-T-FIN-VAR-LIB-Analyst-RW.json
}

# Federated IAM Role
###################################################################################################
module "Ally-ADFS-T-FIN-VAR-LIB-Analyst-RW" {
  source = "../../../modules/FederatedVerticalRole"

  role_name               = "Ally-ADFS-T-FIN-VAR-LIB-Analyst-RW"
  role_description        = "[Terraform] Ally-ADFS-T-FIN-VAR-LIB-Analyst-RW - NonProd"
  role_policy_attachments = [aws_iam_policy.Ally-ADFS-T-FIN-VAR-LIB-Analyst-RW.arn]
  tags = {
    owner          = "CIAPlatformManagement@Ally.com"
    application_id = "105211"
  }
}
