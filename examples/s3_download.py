#!/usr/bin/env python
# -*- coding: utf-8 -*-

import boto3

saml_profile_name = 'saml'
s3_bucket_name = 'eda-tf-aws-analytics-s3-dev-f-innov-us-east-1-workspaces'
src_file_key = 'user/AROAUHJVHXJWSHYYCJQ2W:X_FZJHZZ/X_FZJHZZ.txt'
tgt_file_path = './samplefile.txt'

my_session = boto3.session.Session(profile_name=saml_profile_name)
my_s3_client = my_session.resource('s3')
my_s3_client.Bucket(s3_bucket_name).download_file(src_file_key, tgt_file_path)
